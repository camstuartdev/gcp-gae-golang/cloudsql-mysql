package main

import (
	"net/http"

	humanize "github.com/dustin/go-humanize"
	"github.com/gin-gonic/gin"
)

func statusHandler(c *gin.Context) {
	var err error

	var count uint
	err = db.Table("test").Count(&count).Error
	if err != nil {
		logger.Fatalf("unable to count test records: %s", err.Error())
	}
	c.JSON(http.StatusOK, gin.H{
		"status":         "OK",
		"serviceID":      serviceID,
		"serviceStarted": humanize.Time(startedAt),
		"count":          count,
	})
}

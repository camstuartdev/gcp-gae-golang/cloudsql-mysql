package main

// TestRecord describes the data structure for an Ethereum wallet address
// Represented as a database table, and json response from etherescan
type TestRecord struct {
	TestField string `gorm:"column:test_field;type:varchar(255)" json:"test_field,omitempty"`
}

// TableName sets the default table name
func (TestRecord) TableName() string {
	return "test"
}

package main

import (
	"os"
	"time"

	_ "github.com/GoogleCloudPlatform/cloudsql-proxy/proxy/dialers/mysql"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	logging "github.com/op/go-logging"
)

var (
	serviceID = "cloudsql-mysql"
	router    *gin.Engine
	logger    = logging.MustGetLogger("main")
	startedAt = time.Now()
	db        *gorm.DB
)

func init() {
	var err error
	logFormatter := logging.MustStringFormatter(`%{color}%{time:15:04:05.000} %{shortfunc} ▶ %{level:.10s} %{id:03x}%{color:reset} %{message}`)
	logging.SetFormatter(logFormatter)
	consoleBackend := logging.NewLogBackend(os.Stdout, "", 0)
	consoleBackend.Color = true
	logging.SetLevel(logging.DEBUG, "main")

	dsn := mustGetenv("DSN")
	db, err = gorm.Open("mysql", dsn)
	if err != nil {
		logger.Fatalf("unable to connect to dsn: %s with error: %s", dsn, err.Error())
	}

	var tables []interface{}
	tables = append(tables, &TestRecord{})
	for i := range tables {
		err = db.AutoMigrate(tables[i]).Error
		if err != nil {
			logger.Fatalf("unable to automigrate table with error: %s", err.Error())
		}
	}

	router = gin.Default()
	router.Use(gin.Logger())

	config := cors.DefaultConfig()
	config.AllowOrigins = []string{"http://localhost:4200"}
	router.Use(cors.New(config))

	router.GET("/status", statusHandler)
}

func main() {
	router.Run()
}
